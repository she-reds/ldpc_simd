This repository contains:
- Fast\_LDPC\_decoder\_for\_x86: Testbench tool used for profiling LDPC decoding.
- ldpc-dvbs2-decoder: Library for optimized DVB-S2 decoding.

# Building testbench and library (Build tree)

## Dependencies

* OpenMP

## Build

Then,

```
mkdir build
cd build
cmake ..
make
```

You can then use the Fast\_LDPC\_decoder\_for\_x86/main.icc executable.

If you want to use different coderates, please select the desired value using `cmake-gui ..`.

## Running tests

Type the following command to run the tests:

`ctest`

# Building and installing the ldpc-dvbs2-decoder library only

This method is usefull if you want to use the ldpc-dvbs2-decoder in your system.

Please have a look to ldpc-dvbs2-decoder/README.md

# Building testbench using installed ldpc-dvbs2-decoder library (Install tree)

Please install the ldpc-dvbs2-decoder first, then have a look to Fast\_LDPC\_decoder\_for\_x86/README.md
