get_filename_component(ldpc-dvbs2-decoder_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)

if(NOT TARGET ldpc-dvbs2-decoder)
    include("${ldpc-dvbs2-decoder_CMAKE_DIR}/ldpc-dvbs2-decoderTargets.cmake")
endif()
