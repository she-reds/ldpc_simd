/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of ldpc-dvbs2-decoder.

  ldpc-dvbs2-decoder is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LDPC_DVBS2_DECODER_H
#define LDPC_DVBS2_DECODER_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stddef.h>

/**
 * Error code returned by the library.
 */
typedef enum {
    LDPC_DVBS2_DECODER_ERROR_NONE, /**< No error. */
    LDPC_DVBS2_DECODER_ERROR_RUNTIME, /**< Runtime error. */
    LDPC_DVBS2_DECODER_ERROR_WRONG_PARAMETER /**< Bad arguments. */
} LDPC_DVBS2_DECODER_Error_t;

/**
 * Decoding algorithm type.
 */
typedef enum {
    LDPC_DVBS2_DECODER_SCHEDULER_FLOODED, /**< Flooded algorithm (see sources for ref) */
    LDPC_DVBS2_DECODER_SCHEDULER_LAYERED /**< Layered algorithm (see sources for ref) */
} LDPC_DVBS2_DECODER_Scheduler_t;

/**
 * Decoding coderate
 */
typedef enum {
    LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_32400, /**< 1/2 coderate */
    LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_7200, /**< 8/9 coderate */
    LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_6480 /**< 9/10 coderate */
} LDPC_DVBS2_DECODER_Code_t;

/**
 * Decoder descriptions (do not change values directly)
 *
 */
typedef struct {
    LDPC_DVBS2_DECODER_Scheduler_t scheduler; /**< Decoding algorithm type */
    LDPC_DVBS2_DECODER_Code_t code; /**< Decoding coderate */
    void* decoder; /**< private data */
} LDPC_DVBS2_DECODER_Decoder_t;

/**
 * Init function. Should be called first.
 *
 * @param decoder Pointer to a previously allocated decoder structure.
 * @param scheduler Algorithm type.
 * @param code Coderate used to decode codewords.
 *
 * @return Error code if error, LDPC_DVBS2_DECODER_ERROR_NONE otherwise.
 */
LDPC_DVBS2_DECODER_Error_t LDPC_DVBS2_DECODER_Init(LDPC_DVBS2_DECODER_Decoder_t *decoder,
                          LDPC_DVBS2_DECODER_Scheduler_t scheduler,
                          LDPC_DVBS2_DECODER_Code_t code);

/**
 * Terminate function. Free every allocated resources.
 *
 * @param decoder Decoder to use.
 *
 * @return Error code if error, LDPC_DVBS2_DECODER_ERROR_NONE otherwise.
 */
LDPC_DVBS2_DECODER_Error_t LDPC_DVBS2_DECODER_Terminate(LDPC_DVBS2_DECODER_Decoder_t *decoder);

/**
 * Terminate function. Free every allocated resources.
 *
 * @param decoder Decoder to use.
 * @param input Input codeword. Should be 64800 byte long for DVB-S2.
 * @param output Output codeword. Should be 64800 byte long for DVB-S2.
 * @param iterationCount Number of decoding iterations.
 *
 * @return Error code if error, LDPC_DVBS2_DECODER_ERROR_NONE otherwise.
 */
LDPC_DVBS2_DECODER_Error_t LDPC_DVBS2_DECODER_Decode(LDPC_DVBS2_DECODER_Decoder_t *decoder,
                            char input[],
                            char output[],
                            int iterationCount);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* LDPC_DVBS2_DECODER_H */
