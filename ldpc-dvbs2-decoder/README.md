# Dependancies

- OpenMP (optionnal)
- CMocka (optionnal, for unit-testing only)

# Build and install

```
mkdir build
cd build
cmake ..
make
sudo make install
```

# Running tests

Please install the CMocka framework.

```
ctest
```

# Adding decoding features

ldpc-dvbs2-library is able to decode DVB-S2 codewords with 1/2, 8/9 and 9/10 coderates.
If you want to add coderate support, you have to:
- Generate the table file in the same format than the others, see src/tables/64800x32400-dvb-s2-table.h for instance (coderate 1/2)
- Copy this table file under the src/tables directory
- Instanciate the decoders to use your table by creating two files for each:
  + src/flooded-decoder-\<TABLE\_NAME\>-dvb-s2.c: implementation of the flooded decoder, must include decoders/flooded/flooded-decoder-common.c
  + src/flooded-decoder-\<TABLE\_NAME\>-dvb-s2.h: declare every functions to use the decoder
  + src/layered-decoder-\<TABLE\_NAME\>-dvb-s2.c: implementation of the layered decodere, must include decoders/layered/layered-decoder-common.c
  + src/layered-decoder-\<TABLE\_NAME\>-dvb-s2.h: declare every functions to use the decoder
- Add them to the \_sources variable in the CMakeLists.txt file
- Modify the src/ldpc-dvbs2-decoder.c file to add the call of your previously added functions for both implementations
- Improve the following unit test files (of course =]):
  + test/unit/decoder/flooded/flooded-decoder-test.c
  + test/unit/decoder/layered/layered-decoder-test.c
