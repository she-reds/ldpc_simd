/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of ldpc-dvbs2-decoder.

  ldpc-dvbs2-decoder is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FLOODED_DECODER_64800X6480_DVB_S2_H
#define FLOODED_DECODER_64800X6480_DVB_S2_H

#include "ldpc-dvbs2-decoder/ldpc-dvbs2-decoder.h"

LDPC_DVBS2_DECODER_Error_t LDPC_DVBS2_DECODER_flooded_64800x6480_dvb_s2_Init(LDPC_DVBS2_DECODER_Decoder_t* decoder);
void LDPC_DVBS2_DECODER_flooded_64800x6480_dvb_s2_Terminate(LDPC_DVBS2_DECODER_Decoder_t* decoder);
void LDPC_DVBS2_DECODER_flooded_64800x6480_dvb_s2_Decode(LDPC_DVBS2_DECODER_Decoder_t* decoder,
                                     char var_nodes[],
                                     char Rprime_fix[],
                                     int nombre_iterations);

#endif /* FLOODED_DECODER_64800X6480_DVB_S2_H */
