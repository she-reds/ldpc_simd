/**
  Copyright (c) 2012-2015 "Bordeaux INP, Bertrand LE GAL"
  [http://legal.vvv.enseirb-matmeca.fr]

  This file is part of LDPC_C_Simulator.

  LDPC_C_Simulator is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONSTANTES
#define CONSTANTES

#include <math.h>

#define NB_DEGRES            2

#define _N                   4 // Nombre de Variables
#define _K                   2 // Nombre de Checks   
#define _M                   5 // Nombre de Messages 

#define NOEUD   _N
#define MESSAGE _M

#define NmoinsK     (_N-_K)

#define DEG_1                3
#define DEG_2                2

#define DEG_VN_MAX           2

#define DEG_1_COMPUTATIONS   1
#define DEG_2_COMPUTATIONS   1

#define NB_ITERATIONS        20
#define NB_BITS_VARIABLES    8 //8 ???
#define NB_BITS_MESSAGES     6 //6 ???
#define SAT_POS_VAR  ( (0x0001<<(NB_BITS_VARIABLES-1))-1)
#define SAT_NEG_VAR  (-(0x0001<<(NB_BITS_VARIABLES-1))+1)
#define SAT_POS_MSG  ( (0x0001<<(NB_BITS_MESSAGES -1))-1)
#define SAT_NEG_MSG  (-(0x0001<<(NB_BITS_MESSAGES -1))+1)

#endif


#ifndef _PosNoeudsVariable_
#define _PosNoeudsVariable_
const unsigned short PosNoeudsVariable[5] ={
/* msg =      0, deg =  3 */     0,    1,    2,
/* msg =      1, deg =  2 */     1,    3
};
#endif

